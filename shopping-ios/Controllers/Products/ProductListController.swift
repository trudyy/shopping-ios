//
//  ProductListController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-22.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData
import Toast_Swift

class ProductListController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var productList: [ProductList] = [ProductList]()
    var currentProduct: ProductList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadProducts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindFromProductDetail(segue: UIStoryboardSegue) {
        //todo: goto Cart
//        self.navigationController?.tabBarController?.selectedIndex = 2
//        self.tabBarController?.selectedIndex = 2
    }
    
    func loadProducts() {
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do {
            let productFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductList")
            productList = try context.fetch(productFetch) as! [ProductList]
            print("PRODUCTS: \(productList.count)")
        } catch {
           print("Fetching Failed")
        }
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProductDetails",
            let nav = segue.destination as? UINavigationController, let controller: ProductDetailViewController = nav.topViewController as? ProductDetailViewController {
            if let product = self.currentProduct {
                controller.product = product
            } else {
                print("ERROR current product is nil")
            }
        }
    }
    
}

extension ProductListController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductItemCollectionViewCell else {
            fatalError("Unable to instantiate ProductCell")
        }
        cell.setup(productList[indexPath.row])
        // Configure the cell
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentProduct = productList[indexPath.row]
        performSegue(withIdentifier: "ProductDetails", sender: self)
    }

}

