//
//  ProductItemCollectionViewCell.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-24.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit

class ProductItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func setup(_ product: ProductList) {
        productName.text = product.productName
        productPrice.text = String(format: "$%.2f", product.price)
    }
}
