//
//  ProductDetailViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-25.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Toast_Swift
import CoreData

class ProductDetailViewController: UIViewController {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescriptio: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productCartQty: UITextField!
    @IBOutlet weak var submitToCart: UIButton!
    
    var product: ProductList!
    var user: UserAccount?
    
    var cartProduct: UserProduct?
    var cartQty: Int = 0 {
        didSet {
            if cartQty == 0 {
                submitToCart.setTitle("Add to Cart", for: .normal)
            } else {
                submitToCart.setTitle("Update Cart", for: .normal)
            }
        }
    }

    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext

    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addToCart(_ sender: UIButton) {
        if cartQty == 0 {
            addToCart()
        } else {
            updateQty()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("name: \(product.productName)")
        print("soldBy: \(product.soldBy)")
        
        productName.text = product.productName
        productDescriptio.text = product.productDes
        productPrice.text = String(format: "$%.2f", product.price)
        productCartQty.text = "\(cartQty)"
    }

    override func viewWillAppear(_ animated: Bool) {
        fetchUser()
        fetchProductQty()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchProductQty() {
        if let _ = product {
            let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
            
            // Check the quantity
            do {
                let qtyFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProduct")
                var predicates = [NSPredicate]()
                predicates.append(NSPredicate(format: "user.name == %@", (user?.name)!))
                predicates.append(NSPredicate(format: "product.id = %i", product.id))
                predicates.append(NSPredicate(format: "paid == false"))
                qtyFetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
                let userProducts = try context.fetch(qtyFetch) as! [UserProduct]
                
                if userProducts.count <= 1, let product = userProducts.first {
                    cartProduct = product
                    cartQty = Int(product.qty)
                    productCartQty.text = "\(cartQty)"
                    print("Previous QTY: \(cartQty)")
                } else {
                    print("There should only be one")
                }
            } catch {
                print("Fetch error")
            }
            
        } else {
            showToast("Error retrieving product")
        }
    }
    
    func fetchUser() {
        if let userName = UserDefaults.standard.string(forKey: "name") {
            do {
                let accountFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
                accountFetch.predicate = NSPredicate(format: "name == %@", userName)
                let results = try context.fetch(accountFetch) as! [UserAccount]
                user = results.first
            } catch {
                showToast("Error retrieving user from core data")
            }
            
        } else {
            showToast("Error retrieving user")
        }
    }
    
    func updateQty() {
        if let qty = productCartQty.text {
            let num = (qty as NSString).intValue
            
            if num == 0 {
               // Basically removing the item then
                verifyDelete()
            } else {
                cartProduct?.setValue(num, forKey: "qty")
                cartProduct?.setValue(false, forKey: "paid")
                
                // Save the data to coredata
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                
                self.performSegue(withIdentifier: "unwindFromProductDetail", sender: self)
            }
        }
    }
    
    func verifyDelete() {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to remove this item?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            if let prod = self.cartProduct {
                self.context.delete(prod)
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                
                self.performSegue(withIdentifier: "unwindFromProductDetail", sender: self)
            } else {
                print("Error finding product to delete")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func addToCart() {
        
        // Either create new entry or update the quantity
        if cartQty > 0 {
            // cartProduct?.setValue(user, forKey: "user")
            // cartProduct?.setValue(product, forKey: "product")
            cartProduct?.setValue(cartQty + 1, forKey: "qty")
            cartProduct?.setValue(false, forKey: "paid")

        } else {
            let userproduct = UserProduct(context: context)
            userproduct.user = user
            userproduct.product = product
            userproduct.qty = 1
            userproduct.paid = false
        }
        
        // Save the data to coredata
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        self.performSegue(withIdentifier: "unwindFromProductDetail", sender: self)
    }
    
    func showToast(_ msg: String, _ title: String?=nil, _ completion: ((Bool)->Void)?=nil) {
        // create a toast style
        var style = ToastStyle()
        style.titleAlignment = .center
        style.messageAlignment = .center
        self.view.makeToast(msg, duration: 3.0, position: .center, title: title, image: nil, style: style, completion: completion)
    }
}
