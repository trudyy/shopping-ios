//
//  ShoppingCartViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-21.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData
import Toast_Swift

class ShoppingCartViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var cartItems: [UserProduct] = [UserProduct]()
    var cartSubtotal: Double = 0.00
    var cartTax: Double = 0.00
    var cartTotal: Double = 0.00
    var cartShipping: Double = 5.00

    var editingProduct: UserProduct?
    
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext

    @IBAction func goToCheckout(_ sender: UIBarButtonItem) {
        if cartSubtotal == 0 {
            showToast("Nothing in your cart yet!", "Cannot Proceed to Checkout")
        } else {
            performSegue(withIdentifier: "GoToCheckout", sender: self)
        }
    }
    
    @IBAction func unwindFromProductDetail(segue: UIStoryboardSegue) {
        print("unwindFromProductDetail")
        loadCart()
        calculateSummary()
    }
    
    @IBAction func unwindFromCheckout(segue: UIStoryboardSegue) {
        print("unwindFromCheckout")
        loadCart()
        calculateSummary()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "ShoppingCartTableViewCell", bundle: nil), forCellReuseIdentifier: "ShoppingCartTableViewCell")
        tableView.register(UINib(nibName: "ShoppingCartTotalCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ShoppingCartTotalCell")
        
        tableView.separatorStyle = .none
        
    }

    override func viewWillAppear(_ animated: Bool) {        
        loadCart()
        calculateSummary()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadCart() {
        guard let userName = UserDefaults.standard.string(forKey: "name") else {
            print("Error retrieving user")
            return
        }

        do {
            let cartFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProduct")
            var predicates = [NSPredicate]()
            predicates.append(NSPredicate(format: "user.name == %@", userName))
            predicates.append(NSPredicate(format: "paid == false"))
            cartFetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            cartItems = try context.fetch(cartFetch) as! [UserProduct]
            print("CART: \(cartItems.count)")
        } catch {
            print("Fetching Failed")
        }
        tableView.reloadData()
    }
    
    func calculateSummary() {
        cartSubtotal = 0.00
        for item in cartItems {
            cartSubtotal += Double(item.qty) * (item.product?.price)!
        }
        cartTax = cartSubtotal * 0.07
        cartTotal = cartSubtotal + cartTax + cartShipping
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToCheckout",
            let nav = segue.destination as? UINavigationController, let controller: CheckoutViewController = nav.topViewController as? CheckoutViewController {
            controller.subtotal = cartSubtotal
            controller.tax = cartTax
            controller.total = cartTotal
            controller.deliveryFee = cartShipping
            
        } else if segue.identifier == "CartProductDetails",
            let nav = segue.destination as? UINavigationController, let controller: ProductDetailViewController = nav.topViewController as? ProductDetailViewController {
            if let userproduct = self.editingProduct {
                controller.product = userproduct.product
            } else {
                print("ERROR current product is nil")
            }
        }
    }
    
    func showToast(_ msg: String, _ title: String?=nil) {        
        // create a toast style
        var style = ToastStyle()
        style.titleAlignment = .center
        style.messageAlignment = .center
        self.view.makeToast(msg, duration: 3.0, position: .center, title: title, image: nil, style: style, completion: nil)
    }
    
}

extension ShoppingCartViewController: UITableViewDelegate, UITableViewDataSource {

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartTableViewCell", for: indexPath) as! ShoppingCartTableViewCell
        cell.setup(cartItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alert = UIAlertController(title: "Delete", message: "Are you sure you want to remove this item?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                self.context.delete(self.cartItems[indexPath.row])
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
                
                self.cartItems.remove(at: indexPath.row)
                self.tableView.reloadData()
                self.calculateSummary()
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ShoppingCartTotalCell") as! ShoppingCartTotalCell
        cell.setup(subtotal: cartSubtotal, tax: cartTax, total: cartTotal, shipping: cartShipping)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.editingProduct = cartItems[indexPath.row]
        self.performSegue(withIdentifier: "CartProductDetails", sender: self)
    }
}
