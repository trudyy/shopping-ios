//
//  ShoppingCartTotalCell.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-26.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit

class ShoppingCartTotalCell: UITableViewHeaderFooterView {

    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var taxtotalLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(subtotal: Double, tax: Double, total: Double, shipping: Double) {
        subtotalLabel.text = String(format: "$%.2f", subtotal)
        taxtotalLabel.text = String(format: "$%.2f", tax)
        shippingLabel.text = String(format: "$%.2f", shipping)
        totalLabel.text = String(format: "$%.2f", total)
    }
}
