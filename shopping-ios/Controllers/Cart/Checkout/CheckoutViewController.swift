//
//  CheckoutViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-26.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Toast_Swift
import CoreData

class CheckoutViewController: UIViewController {

    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    
    @IBOutlet weak var textCardNumber: UITextField!
    @IBOutlet weak var textCardName: UITextField!
    @IBOutlet weak var textExpireDate: UITextField!
    @IBOutlet weak var textSecureCode: UITextField!
    
    var subtotal: Double?
    var tax: Double?
    var total: Double?
    var deliveryFee: Double?
    
    var card: CreditAccount?
    var user: UserAccount?
    var newCard: Card?
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func processPayment(_ sender: UIButton) {
        if !self.isValidCreditCard() {
            return
        } else {
            let alert = UIAlertController(title: "Proceed with Payment", message: "Are you sure?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
                self.processPayment()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let subtotal = subtotal {
            subtotalLabel.text = String(format: "$%.2f", subtotal)
        }
        if let tax = tax {
            taxLabel.text = String(format: "$%.2f", tax)
        }
        if let deliveryFee = deliveryFee {
            shippingLabel.text = String(format: "$%.2f", deliveryFee)
        }
        if let total = total {
            totalLabel.text = String(format: "$%.2f", total)
        }
        fetchUser()
        fetchCredit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func validate() -> Bool {
//        // check credit card fields
//        if (card != nil ){
//            if( card?.cardNumber != nil && card?.nameOnCard != nil && card?.secureCode != nil){
//                if (card?.cardNumber == "" || card?.nameOnCard == "" && card?.secureCode == ""){
//                    self.showToast("Missing credit card information. Please update your profile")
//                    return false
//                } else if( (card?.expireDate)! < Date()){
//                    self.view.makeToast("Your credit card has been expired. Pleae update your profile")
//                    return false
//                } else {
//                    return true
//                }
//            }else {
//                self.showToast("Missing credit card information. Please update your profile")
//                return false
//            }
//
//        } else {
//            self.showToast("Missing credit card information. Please update your profile")
//            return false
//        }
//    }
    
    func canUpdateCreditCart() -> Bool {
        if isValidCreditCard(){
            if (card != nil) {
                
            } else {
                let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
                card = CreditAccount(context: context)
            }
            card?.nameOnCard = newCard?.name
            card?.cardNumber = newCard?.cardNumber
            card?.expireDate = newCard?.expireDate
            card?.secureCode = newCard?.secureCode
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            self.view.makeToast("Credit card information has been saved.",position: .center)
            return true
        }
        return false
    }
    
    func processPayment() {

        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        
        if let userName = UserDefaults.standard.string(forKey: "name") {

            do {
                let qtyFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProduct")
                var predicates = [NSPredicate]()
                predicates.append(NSPredicate(format: "user.name == %@", userName))
                predicates.append(NSPredicate(format: "paid == false"))
                qtyFetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
                let cartProducts = try context.fetch(qtyFetch) as! [UserProduct]
                
                for cartProduct in cartProducts {
//                    print("product title: \(cartProduct.product?.productName ?? "N/A")")
//                    print("product qty: \(cartProduct.qty)")
//                    print("product date paid: \(cartProduct.date_paid)")
                    cartProduct.setValue(true, forKey: "paid")
                    cartProduct.setValue(Date(), forKey: "date_paid")
                }
                
                // Save the data to coredata
                (UIApplication.shared.delegate as! AppDelegate).saveContext()

                self.showToast("Thank you for your purchase!") { didTap in
                    self.performSegue(withIdentifier: "unwindFromCheckout", sender: self)
                }
                
            } catch {
                print("Error fetching user products!")
            }
        }
    }
    
    func showToast(_ msg: String, _ title: String?=nil, _ completion: ((Bool)->Void)?=nil) {
        // create a toast style
        var style = ToastStyle()
        style.titleAlignment = .center
        style.messageAlignment = .center
        self.view.makeToast(msg, duration: 3.0, position: .center, title: title, image: nil, style: style, completion: completion)
    }
    
    //fetch user information
    func fetchUser(){
        var personName: String?
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        if let name = UserDefaults.standard.string(forKey: "name"){
            personName = name
        } else {
            print("There is an issue")
        }
        do{
            let userFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
            userFetch.predicate = NSPredicate(format: "name == %@", personName!)
            let results = try context.fetch(userFetch) as! [UserAccount]
            self.user = results.first!
        } catch {
            print ("Failed to fetch data")
        }
        
    }
    
    //fetch credit card information
    func fetchCredit(){
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do{
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CreditAccount")
            fetch.predicate = NSPredicate(format: "creditAccount.name == %@", (user?.name)!)
            let results = try context.fetch(fetch) as! [CreditAccount]
            if let aCard = results.first {
                self.card = aCard
                
                if card != nil {
                    textCardNumber?.text = card?.cardNumber
                    textCardName?.text = card?.nameOnCard
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/YY"
                    let dateString = dateFormatter.string(from: (card?.expireDate)!)
                    textExpireDate?.text = dateString
                    textSecureCode?.text = card?.secureCode
                    return
                }
            }
            
        } catch {
            print ("Failed to fetch data")
        }
        
        showToast("No credit card information stored in the system")

    }
   
    func isValidCreditCard() -> Bool{
        newCard = Card()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM/YY"
        if textExpireDate != nil {
            if let newDate = dateformatter.date(from:textExpireDate.text!){
                newCard?.expireDate = newDate
            } else {
                 self.showToast("Date in wrong format. Please update your profile")
                return false
                
            }
        } else {
            self.showToast("Date in wrong format. Please update your profile")
            return false
        }
        
        newCard?.cardNumber = textCardNumber?.text
        newCard?.name = textCardName?.text
        newCard?.secureCode = textSecureCode?.text
        if(newCard?.cardNumber != nil && newCard?.name != nil && newCard?.secureCode != nil ){
            if (newCard?.name == "" || newCard?.cardNumber == "" || newCard?.secureCode == "" ){
                self.showToast("Please enter all payment information.")
                return false
                
            } else if (Int((newCard?.cardNumber)!) == nil) {
                self.showToast("Card number is invalid")
                return false
                
            } else if ((newCard?.expireDate)! < Date()){
                self.showToast("Your card is expired, please enter new card information")
                return false
                
            } else {
                return true
            }
            
            
        }else{
            self.showToast("Please enter all payment information.")
            return false
        }
    }
}
