//
//  ShoppingCartTableViewCell.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-25.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit

class ShoppingCartTableViewCell: UITableViewCell {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productQty: UILabel!
    @IBOutlet weak var productUnit: UILabel!
    @IBOutlet weak var productTotal: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(_ item: UserProduct) {
        if let product = item.product {
            productName.text = product.productName
            productDescription.text = product.productDes
            productQty.text = "x \(item.qty)"
            productUnit.text = String(format: "$%.2f", product.price)
            productTotal.text = String(format: "$%.2f", product.price * Double(item.qty))
        }
    }
}
