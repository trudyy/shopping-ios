//
//  UserProfileController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-22.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData
import Eureka


    class UserProfileController: FormViewController {

        
        @IBAction func saveProfile(_ sender: UIBarButtonItem) {
            let name: String = UserDefaults.standard.string(forKey: "name")!
            let aperson = Person()
            aperson.name = name
            aperson.email = form.rowBy(tag: "email")?.value!
            aperson.address = form.rowBy(tag: "address")?.value!
            aperson.city = form.rowBy(tag: "city")?.value!
            aperson.postalCode = form.rowBy(tag: "postal")?.value!
            aperson.province = form.rowBy(tag: "province")?.value!
            aperson.phoneNumber = form.rowBy(tag: "province")?.value!
	
            if(isValid(person: aperson)){
                let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
                do{
                    let accountFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
                    accountFetch.predicate = NSPredicate(format: "name == %@", aperson.name!)
                    let results = try context.fetch(accountFetch) as! [UserAccount]
                    if let aUser = results.first{
                        aUser.setValue(aperson.email, forKey: "email")
                        aUser.setValue(aperson.address, forKey: "address")
                        aUser.setValue(aperson.city, forKey: "city")
                        aUser.setValue(aperson.postalCode, forKey: "postal")
                        aUser.setValue(aperson.province, forKey: "province")
                        aUser.setValue(aperson.phoneNumber, forKey: "phoneNumber")
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }else{
                        //lblMessage.text! = "Wrong password"
                        print("Fail to get data")
                    }
                }catch {
                    // lblMessage.text! = "Wrong user name"
                    print("Fail to get data")
                }

            }
            //self.delegate?.saveNewData()
        }
        //  @IBAction func saveProfile(_ sender: UIBarButtonItem) {

        //
        //print("Saving profile info. WIP")
    //}
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let person = Person()
        if let data = UserDefaults.standard.string(forKey: "name") {
            person.name = data
            
        } else {
        print("There is an issue")
        }
        
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do{
            let accountFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
            accountFetch.predicate = NSPredicate(format: "name == %@", person.name!)
            let results = try context.fetch(accountFetch) as! [UserAccount]
            
            
            if let aUser = results.first{
            person.email = aUser.email
            person.address = aUser.address
            person.postalCode = aUser.postal
            person.phoneNumber = aUser.phoneNumber
            person.city = aUser.city
            person.province = aUser.province
            
            }else{
            //lblMessage.text! = "Wrong password"
            // lblMessage.text! = "Wrong user name"
            print("Fail to get data")
            }
        
        }catch {
            print("Failed to open core data")
        }
        
        form
        +++ Section("User Profile Information")
        <<< TextRow(){ row in
        row.title = "Name"
        row.value = person.name
        row.placeholder = "Some Name"
        }
        <<< TextRow(){ row in
        row.title = "Email"
        row.value = person.email
        row.tag = "email"
        row.placeholder = "test@test.com"
        }
        <<< TextRow(){ row in
        row.title = "Address"
        row.value = person.address
        row.tag = "address"
        row.placeholder = "Some Address"
        }
        <<< TextRow(){ row in
        row.title = "City"
        row.value = person.city
        row.tag = "city"
        row.placeholder = "Vancouver"
        }
        <<< TextRow(){ row in
        row.title = "Province"
        row.value = person.province
        row.tag = "province"
        row.placeholder = "British Columbia"
        }
        <<< TextRow(){ row in
        row.title = "Postal Code"
        row.value = person.postalCode
        row.tag = "postal"
        row.placeholder = "123456"
        }
        <<< PhoneRow(){
        $0.title = "Phone"
        $0.tag = "phone"
        $0.value = person.phoneNumber
        $0.placeholder = "604-123-4567"
        }
    }
        
        func isValid(person: Person?) -> Bool{
            if ( person?.email != nil && person?.city != nil && person?.address != nil && person?.province != nil && person?.postalCode != nil && person?.phoneNumber != nil)
            {
                if (person?.email == "" || person?.city == "" || person?.address == "" || person?.province == "" || person?.postalCode == "" || person?.phoneNumber == ""){
                    self.view.makeToast("Please fill all information" ,position: .center)
                    return false
                }
            }else {
                self.view.makeToast("Please fill all information" ,position: .center)
                return false
            }
            
            return true
        }
        

    override func didReceiveMemoryWarning()	 {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
