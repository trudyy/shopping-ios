//
//  UserCreditCardController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-10.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Eureka
import CoreData


class UserCreditCardController: FormViewController {
    
    
    var personName: String! = ""
    
    var newCard: Card?
    var card : CreditAccount?
    var user : UserAccount?
    
    @IBAction func saveCard(_ sender: UIBarButtonItem) {
        newCard = Card()
        newCard?.cardNumber = form.rowBy(tag: "number")?.value
        newCard?.expireDate = form.rowBy(tag: "date")?.value
        newCard?.name = form.rowBy(tag: "name")?.value
        newCard?.secureCode = form.rowBy(tag: "secureCode")?.value
        
        if(checkValid()){
            if (card != nil ){
                
            }else {
                let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
                card = CreditAccount(context: context)
            }
            
            card?.cardNumber = newCard?.cardNumber
            card?.creditAccount = user
            card?.nameOnCard = newCard?.name
            card?.secureCode = newCard?.secureCode
            card?.expireDate = newCard?.expireDate
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
             self.view.makeToast("Credit card information has been saved.",position: .center)
        }
    //print("Saving card info. WIP")
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUser()
        fetchCredit()
        let dateformat = DateFormatter()
        dateformat.setLocalizedDateFormatFromTemplate("MM/YY")
        
        form
            +++ Section("Credit Card Information")
            <<< TextRow(){ row in
                row.title = "Name on card"
                row.tag = "name"
                if let user = user {
                    row.value = user.name
                }
            }
            <<< TextRow(){ row in
                row.title = "Card number"
                row.tag = "number"
                row.placeholder = "**** **** **** ****"
            }
            <<< DateRow(){ row in
                row.title = "Expiry date"
                row.tag = "date"
                row.value = Date(timeIntervalSinceReferenceDate: 0)
                row.dateFormatter = dateformat
            }
            <<< PasswordRow(){ row in
                row.title = "Secure code"
                row.tag = "secureCode"
                row.placeholder = "***"
            }
        
       // print(card?.cardNumber!!)
            form.rowBy(tag: "number")?.value = card?.cardNumber
            form.rowBy(tag: "date")?.value = card?.expireDate
            form.rowBy(tag: "secureCode")?.value = card?.secureCode
            form.rowBy(tag: "name")?.value = card?.nameOnCard
        
    }
    
    func fetchUser(){
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        if let name = UserDefaults.standard.string(forKey: "name"){
            self.personName = name
        } else {
            print("There is an issue")
        }
        do{
            let userFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
            userFetch.predicate = NSPredicate(format: "name == %@", personName)
            let results = try context.fetch(userFetch) as! [UserAccount]
            self.user = results.first!
        } catch {
            print ("Failed to fetch data")
        }
        
    }
    func fetchCredit(){
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do{
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CreditAccount")
            fetch.predicate = NSPredicate(format: "creditAccount.name == %@", (user?.name)!)
            let results = try context.fetch(fetch) as! [CreditAccount]
            if let aCard = results.first {
                self.card = aCard
            } else {
                print ("Fail to get data")
            }
            
        } catch {
            print ("Failed to fetch data")
        }
        
    }
    func checkValid() -> Bool{
        //8print(newCard?.expireDate!)
        if(newCard?.cardNumber != nil && newCard?.name != nil && newCard?.secureCode != nil ){
            if( newCard?.cardNumber == "" || newCard?.name == "" || newCard?.secureCode == "")
            {
                self.view.makeToast("Please fill all the information",position: .center)
                return false
            } else if( (newCard?.expireDate)! < Date()){
                self.view.makeToast("Expire date must be greater than today",position: .center)
                return false
            }else {
                return true
            }
        }else {
            self.view.makeToast("Please fill all the information",position: .center)
            return false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

