//
//  AddOrderViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-19.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Eureka
import Toast_Swift

class AddProductViewController: FormViewController {
    
    var productTitle: String?
    var productDescription: String?
    var productPrice: String?
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveProduct(_ sender: UIBarButtonItem) {
        if validate() {
            saveProduct()
        } else {
            // create a toast style
            var style = ToastStyle()
            style.titleAlignment = .center
            style.messageAlignment = .center
            
            self.view.makeToast("Please ensure all fields are filled", duration: 3.0, position: .center, title: "Error", image: nil, style: style, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        form
            +++ Section("New Product Information")
            <<< TextRow(){ row in
                row.title = "Title"
                row.tag = "title"
                row.placeholder = "Item Name"
            }
            <<< TextRow(){ row in
                row.title = "Description"
                row.tag = "description"
                row.placeholder = "Some Description"
            }
            <<< DecimalRow(){ row in
                row.title = "Price"
                row.tag = "price"
                row.placeholder = "0.00"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validate() -> Bool {
        guard let titleRow: TextRow = form.rowBy(tag: "title"), let title = titleRow.value, !title.isEmpty else {
            print("No title given")
            return false
        }
        guard let descriptionRow: TextRow = form.rowBy(tag: "description"), let description = descriptionRow.value, !description.isEmpty else {
            print("No description given")
            return false
        }
        guard let priceRow: DecimalRow = form.rowBy(tag: "price"), let price = priceRow.value, !price.isNaN  else {
            print("No price given")
            return false
        }

        productTitle = title
        productDescription = description
        productPrice = "\(price)"
        return true
    }
    
    func saveProduct() {
        guard let userName = UserDefaults.standard.string(forKey: "name") else {
            print("Error retrieving user")
            return
        }

        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        let product = ProductList(context: context)
        product.id = Int64(NSDate().timeIntervalSince1970)
        print("hash: \(product.id)")
        product.productName = productTitle
        product.productDes = productDescription
        product.price = (productPrice! as NSString).doubleValue
        product.soldBy = userName
        
        // Save the data to coredata
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        self.dismiss(animated: true, completion: nil)
    }
}
