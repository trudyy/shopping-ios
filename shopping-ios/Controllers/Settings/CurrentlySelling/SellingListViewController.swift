//
//  OrdersViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-19.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData
import Toast_Swift

class SellingListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var sellingList: [ProductList] = [ProductList]()

    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext

    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        loadSellingProducts()
    }

    override func viewWillAppear(_ animated: Bool) {
        loadSellingProducts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadSellingProducts() {
        guard let userName = UserDefaults.standard.string(forKey: "name") else {
            print("Error retrieving user")
            return
        }
        
        do {
            let productFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "ProductList")
            productFetch.predicate = NSPredicate(format: "soldBy == %@", userName)
            sellingList = try context.fetch(productFetch) as! [ProductList]
            print("SELLING PRODUCTS: \(sellingList.count)")
        } catch {
            print("Fetching Failed")
        }
        collectionView.reloadData()
    }
    
}

extension SellingListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sellingList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductItemCollectionViewCell else {
            fatalError("Unable to instantiate ProductCell")
        }
        cell.setup(sellingList[indexPath.row])
        // Configure the cell
        return cell
    }
    
}
