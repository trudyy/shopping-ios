//
//  PurchasedListViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-21.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData

struct PurchaseBundle {
    var date: Date?
    var products: [UserProduct] = [UserProduct]()
    var subtotal: Double = 0.00
    var tax: Double = 0.00
    var shipping: Double = 0.00
    var total: Double = 0.00
}

class PurchasedListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var bundles: [PurchaseBundle] = [PurchaseBundle]()
    var allPurchases: [UserProduct] = [UserProduct]()
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "ShoppingCartTableViewCell", bundle: nil), forCellReuseIdentifier: "ShoppingCartTableViewCell")
        tableView.register(UINib(nibName: "ShoppingCartTotalCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "ShoppingCartTotalCell")

        tableView.separatorStyle = .none
        
    }

    override func viewWillAppear(_ animated: Bool) {
        loadPurchaseHistory()
        groupPurchaseHistory()
        calcPurchaseHistory()
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadPurchaseHistory() {
        guard let userName = UserDefaults.standard.string(forKey: "name") else {
            print("Error retrieving user")
            return
        }
        
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do {
            let cartFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserProduct")
            var predicates = [NSPredicate]()
            predicates.append(NSPredicate(format: "user.name == %@", userName))
            predicates.append(NSPredicate(format: "paid == true"))
            cartFetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            let sortDescriptor = NSSortDescriptor(key: "date_paid", ascending: true)
            let sortDescriptors = [sortDescriptor]
            cartFetch.sortDescriptors = sortDescriptors
            allPurchases = try context.fetch(cartFetch) as! [UserProduct]
        } catch {
            print("Fetching Failed")
        }
    }
    
    func groupPurchaseHistory() {
        for purchase in allPurchases {
            if let index = bundles.index(where: {Calendar.current.isDate($0.date!, equalTo: purchase.date_paid!, toGranularity: .second)}) {
                bundles[index].products.append(purchase)
            } else {
                let bundle = PurchaseBundle(date: purchase.date_paid, products: [purchase], subtotal: 0, tax: 0, shipping: 0, total: 0)
                bundles.append(bundle)
            }
        }
    }
    
    func calcPurchaseHistory() {
        for index in 0..<bundles.count {
            var subtotal = 0.00
            for product in bundles[index].products {
                subtotal += (product.product?.price)! * Double(product.qty)
            }
            bundles[index].subtotal = subtotal
            bundles[index].tax = subtotal * 0.07
            bundles[index].shipping = 5
            bundles[index].total = subtotal + (subtotal * 0.07) + 5
        }
    }
}

extension PurchasedListViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return bundles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bundles[section].products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartTableViewCell", for: indexPath) as! ShoppingCartTableViewCell
        cell.setup(bundles[indexPath.section].products[indexPath.row])
        return cell
    }
        
    // MARK: - Headers

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd (hh:mm)"
        return "Purchased on \(dateFormatter.string(from: bundles[section].date!))"
    }
    
    // MARK: - Footers
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ShoppingCartTotalCell") as! ShoppingCartTotalCell
        let bundle = bundles[section] as PurchaseBundle
        cell.setup(subtotal: bundle.subtotal, tax: bundle.tax, total: bundle.total, shipping: bundle.shipping)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 100
    }
}
