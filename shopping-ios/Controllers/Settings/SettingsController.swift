//
//  SettingsController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-22.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Eureka

class SettingsController: FormViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        form
            +++ Section("User Information")
            <<< LabelRow(){
                $0.title = "Profile"
                $0.onCellSelection({ (cell, row) in
                    self.performSegue(withIdentifier: "GoToProfile", sender: self)
                })
            }
            <<< LabelRow(){
                $0.title = "Credit Card"
                $0.onCellSelection({ (cell, row) in
                    self.performSegue(withIdentifier: "GoToCreditCard", sender: self)
                })
            }
            
            +++ Section("Buying")
            <<< LabelRow(){
                $0.title = "Purchase History"
                $0.onCellSelection({ (cell, row) in
                    self.performSegue(withIdentifier: "GoToPurchasedItems", sender: self)
                })
            }

            +++ Section("Selling")
//            <<< LabelRow(){
//                $0.title = "Items Sold"
//                $0.onCellSelection({ (cell, row) in
//                    self.performSegue(withIdentifier: "GoToSoldItems", sender: self)
//                })
//            }
            <<< LabelRow(){
                $0.title = "Items for Sale"
                $0.onCellSelection({ (cell, row) in
                    self.performSegue(withIdentifier: "GoToCurrentlySelling", sender: self)
                })
            }
            
            +++ Section()
            <<< LabelRow(){
                $0.title = "Logout"
                $0.onCellSelection({ (cell, row) in
                    self.logout()
                })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func logout() {
        self.dismiss(animated: true, completion: {
            print("completed logout")
            self.navigationController?.popToRootViewController(animated: true)
        }
        )
    }
}
