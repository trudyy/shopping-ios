//
//  RegisterViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-07.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import Toast_Swift
import CoreData
import Eureka

class RegisterViewController: FormViewController{

    var delegate: WelcomeDelegate?
    var user: UserAccount?
    var person: Person?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        form
            +++ Section("Register")
            <<< TextRow(){ row in
                row.title = "Username"
                row.tag = "name"
                row.placeholder = "username"
            }
            <<< PasswordRow(){ row in
                row.title = "Password"
                row.tag = "password"
                row.placeholder = "******"
            }
            <<< PasswordRow(){ row in
                row.title = "Confirm"
                row.tag = "password2"
                row.placeholder = "******"
            }
            <<< EmailRow(){ row in
                row.title = "Email"
                row.tag = "email"
                row.placeholder = "comp3912@bcit.com"
            }
            <<< TextRow(){ row in
                row.title = "Address"
                row.tag = "address"
                row.placeholder = "Seymour"
            }
            <<< TextRow(){ row in
                row.title = "City"
                row.tag = "city"
                row.placeholder = "Vancouver"
            }
            <<< TextRow(){ row in
                row.title = "Province"
                row.tag = "province"
                row.placeholder = "BC"
            }
            <<< TextRow(){ row in
                row.title = "Postal Code"
                row.tag = "postal"
                row.placeholder = "123456"
            }
            <<< PhoneRow(){
                $0.title = "Phone Number"
                $0.tag = "phone"
                $0.placeholder = "604-123-4567"
            }
            <<< ButtonRow(){
                $0.title = "Create"
                $0.onCellSelection(self.buttonTapped)
                
        }
                // Do any additional setup after loading the view.
    }
    
    func buttonTapped(cell: ButtonCellOf<String>, row: ButtonRow){
        //Set it properties
        person = Person()
        person?.name = form.rowBy(tag: "name")?.value
        person?.password = form.rowBy(tag: "password")?.value
        person?.email = form.rowBy(tag: "email")?.value
        person?.address = form.rowBy(tag: "address")?.value
        person?.city = form.rowBy(tag: "city")?.value
        person?.province = form.rowBy(tag: "province")?.value
        person?.postalCode = form.rowBy(tag: "postal")?.value
        person?.phoneNumber = form.rowBy(tag: "phone")?.value
        if (checkEntry()){
            let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
            
            //Create a new task object
            self.user = UserAccount(context: context)
            user?.name = person?.name
            user?.password = person?.password
            user?.email = person?.email
            user?.city = person?.city
            user?.address = person?.address
            user?.province = person?.province
            user?.postal = person?.postalCode
            user?.phoneNumber = person?.phoneNumber
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            performSegue(withIdentifier: "goToWelcome", sender: self)
        }
    }

    //check if all information has been entered
    func checkEntry() -> Bool{
        if(person?.name != nil && person?.email != nil && person?.password != nil && person?.address != nil && person?.city != nil && person?.province != nil && person?.phoneNumber != nil && person?.postalCode != nil){
            if (person?.name == "" || person?.email == "" || person?.address == "" || person?.password == "" || person?.city == "" || person?.province == "" || person?.phoneNumber == "" || person?.postalCode == ""){
                self.view.makeToast("Please fill all information" ,position: .center)
                return false
            }
            if (checkAccountExist(name: person?.name)){
                self.view.makeToast("User account exists, choose another name",position: .center)
                return false
            }
            let password: String? = form.rowBy(tag: "password")?.value
            let password2: String? = form.rowBy(tag: "password2")?.value
            let length = password?.characters.count
            if (length! < 6){
                self.view.makeToast("Passwords needs to be at least 6 character long",position: .center)
                return false
                
            }
            if (password != password2){
                self.view.makeToast("Passwords does not match, please enter again",position: .center)
                return false
            }
            return true
        }else{
            self.view.makeToast("Please fill all information" ,position: .center)
            return false
            
        }
    }
    
func checkAccountExist( name: String? ) -> Bool{
        let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
        do {
            let accountFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
            accountFetch.predicate = NSPredicate(format: "name == %@", name!)
            let results = try context.fetch(accountFetch) as! [UserAccount]
                if (results.first != nil ){
                    return true
                }else{
                    return false
                }
        }catch {
                return false
            }
            
    }
    /*
    //MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToLogin",
            let controller: LoginViewController = segue.destination as? LoginViewController {
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
     }
     */

 }
 

