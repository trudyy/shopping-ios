//
//  WelcomeViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-07.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit

protocol WelcomeDelegate {
    func enterSite()
}

class WelcomeViewController: UIViewController, WelcomeDelegate {
    @IBOutlet weak var myImg: UIImageView!
    
    @IBAction func login(_ sender: UIButton) {
        performSegue(withIdentifier: "GoToLogin", sender: self)
    }
    
    @IBAction func register(_ sender: UIButton) {
        performSegue(withIdentifier: "GoToRegister", sender: self)
    }
    
    func enterSite() {
        performSegue(withIdentifier: "GoToSite", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = NSURL(string: "https://showscene.ca/wp-content/uploads/2017/10/welcome.jpg") {
            if let data = NSData(contentsOf: url as URL) {
                myImg.image = UIImage(data: data as Data)
            }
        }        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToRegister",
            let controller: RegisterViewController = segue.destination as? RegisterViewController {
            controller.delegate = self
        } else if segue.identifier == "GoToLogin",
            let controller: LoginViewController = segue.destination as? LoginViewController {
            controller.delegate = self
        }
    }

}
