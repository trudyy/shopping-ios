//
//  LoginViewController.swift
//  shopping-ios
//
//  Created by Trudy on 2017-11-07.
//  Copyright © 2017 bcit. All rights reserved.
//

import UIKit
import CoreData
import Toast_Swift

class LoginViewController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtName: UITextField!
    
    var delegate: WelcomeDelegate?
    

    @IBAction func btnLogin(_ sender: UIButton) {
        
        if (txtName != nil && txtPassword != nil){
            
            let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
            do {
                let accountFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UserAccount")
                accountFetch.predicate = NSPredicate(format: "name == %@", txtName.text!)
                let results = try context.fetch(accountFetch) as! [UserAccount]
                
                if let aUser = results.first{
                    let password = aUser.password
                    if (password == txtPassword.text!){
                        //lblMessage.text! = "Login Successful"
                        UserDefaults.standard.set(txtName.text!, forKey: "name")
                        self.view.makeToast("Login Successful")
                        self.delegate?.enterSite()
                    }else{
                        //lblMessage.text! = "Wrong password"
                        self.view.makeToast("Wrong password")
                    }
                } else{
                    //lblMessage.text! = "Wrong user name"
                    self.view.makeToast("Wrong user name")
                }
            } catch {
                // lblMessage.text! = "Wrong user name"
                self.view.makeToast("Wrong user name")
            }
        }else{
            // lblMessage.text! = "Please enter name and password!"
            self.view.makeToast("Please enter name and password!")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblMessage.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
