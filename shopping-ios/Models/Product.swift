//
//  Product.swift
//  shopping-ios
//
//  Created by Trudy on 2017-10-24.
//  Copyright © 2017 bcit. All rights reserved.
//
import Foundation

class Product: NSObject, NSCoding {
    
    let name: String
    let price: Double
    
    init(name: String) {
        self.name = name
        self.price = 0
    }
    
    init(name: String, price: Double) {
        self.name = name
        self.price = price
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as? String ?? ""
        self.price = decoder.decodeDouble(forKey: "price")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(price, forKey: "price")
    }
    
}
