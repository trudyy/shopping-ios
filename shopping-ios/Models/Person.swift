//
//  Person.swift
//  shopping-ios
//
//  Created by kathymeow on 2017-11-23.
//  Copyright © 2017 bcit. All rights reserved.
//

import Foundation
import UIKit

class Person{
    var name:String?
    var password: String?
    var email:String?
    var address: String?
    var city: String?
    var province: String?
    var postalCode: String?
    var phoneNumber: String?
    init(name:String,password:String,email:String,address:String,city:String,province:String,postalCode:String,phoneNumber:String){
            self.name = name
            self.password = password
            self.email = email
            self.address = address
            self.city = city
            self.province = province
            self.postalCode = postalCode
            self.phoneNumber = phoneNumber
        
        }
    init(){
        
    }
    
    
}
